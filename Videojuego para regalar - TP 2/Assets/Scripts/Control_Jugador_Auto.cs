using UnityEngine;
using UnityEngine.UI;

public class Control_Jugador_Auto : MonoBehaviour
{
    [Range(0, 10)] public float Velocidad;
    [Range(-10, 10)] public float LimiteIzquierda; //= -3.69f;
    [Range(-10, 10)] public float LimiteDerecha; //= 3.457f;
    [Range(0, 10)] public int Vidas = 3;
    public Text TextoVidas;

    void Start()
    {
        ActualizarTextoVida();
    }

    void Update()
    {
        if (Vidas <= 0)
        {
            return;
        }

        float InputMovimiento = Input.GetAxis("Horizontal");
        Vector3 Movimiento = new Vector3(InputMovimiento * Velocidad * Time.deltaTime, 0, 0);
        Vector3 Posicion = transform.position + Movimiento;

        if (Posicion.x < LimiteIzquierda)
        {
            Posicion.x = LimiteIzquierda;
        }
        else if (Posicion.x > LimiteDerecha)
        {
            Posicion.x = LimiteDerecha;
        }

        transform.position = Posicion;
        //transform.Translate(Movimiento);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstaculo"))
        {
            Vidas--;
            if (Vidas < 0)
            {
                Vidas = 0;
            }
            ActualizarTextoVida();
            if (Vidas <= 0)
            {
                Debug.Log("CHOCASTE");
            }
        }
    }

    void ActualizarTextoVida()
    {
        TextoVidas.text = "Vidas: " + Vidas;
    }
}
