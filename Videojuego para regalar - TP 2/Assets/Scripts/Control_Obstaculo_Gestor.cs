using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Obstaculo_Gestor : MonoBehaviour
{
    public GameObject PrefabObstaculo;
    public Transform[] PuntosGeneracion;
    public float DelayGeneracion = 1;
    private GameObject ObstaculoActual;

    void Start()
    {
        StartCoroutine(GenerarObstaculo());
    }

    void Update()
    {
        
    }

    IEnumerator GenerarObstaculo()
    {
        while (true)
        {
            yield return new WaitForSeconds(DelayGeneracion);

            if (ObstaculoActual == null)
            {
                int IndiceAleatorio = Random.Range(0, PuntosGeneracion.Length);
                Transform PuntoGeneracion = PuntosGeneracion[IndiceAleatorio];
                ObstaculoActual = Instantiate(PrefabObstaculo, PuntoGeneracion.position, Quaternion.identity);
            }
        }
    }
}
