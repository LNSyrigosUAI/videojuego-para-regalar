using UnityEngine;

public class Control_Obstaculo : MonoBehaviour
{
    [Range(0, 100)] public float VelocidadCaida;

    void Start()
    {
        
    }

    void Update()
    {
        transform.position += Vector3.down * VelocidadCaida * Time.deltaTime;

        if(transform.position.y< Camera.main.transform.position.y - Camera.main.orthographicSize)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}
